﻿using dTools;
using EasyControl.Service;
using ESBasic;
using OMCS.Contracts;
using OMCS.Passive;
using OMCS.Passive.MultiChat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyControl
{
    public partial class FrmRemoteDesktop : Form
    {
        public FrmRemoteDesktop(string userId)
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw, true);//调整大小时重绘
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);// 双缓冲
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 禁止擦除背景.
            this.SetStyle(ControlStyles.UserPaint, true);//自行绘制            
            this.UpdateStyles();
            //this.desktopConnector.WatchingOnly = false;
            //this.desktopConnector.ConnectEnded += DesktopConnector_ConnectEnded;
            //this.desktopConnector.Disconnected += DesktopConnector_Disconnected;
            this.desktopConnector.BeginConnect(userId);
            //this.microphoneConnector.BeginConnect(userId);

            //初始化视频
            InitializeAudio(userId);
        }
        private IChatGroup chatGroup;
        public void InitializeAudio(string userId)
        {
            var chatGroupID = $"G_{userId}";
            this.chatGroup = Const.IMultimediaManager.ChatGroupEntrance.Join(ChatType.Audio, chatGroupID);
            foreach (IChatUnit unit in this.chatGroup.GetOtherMembers())
            {
                unit.MicrophoneConnector.BeginConnect(unit.MemberID);
            }

            //Const.IMultimediaManager.AudioCaptured += IMultimediaManager_AudioCaptured;
            //Const.IMultimediaManager.AudioPlayed += IMultimediaManager_AudioPlayed;

            //this.chatGroup.SomeoneJoin += ChatGroup_SomeoneJoin;
            //this.chatGroup.SomeoneExit += ChatGroup_SomeoneExit;
        }

        private void ChatGroup_SomeoneExit(string obj)
        {
            Console.WriteLine($"ChatGroup_SomeoneExit:{obj}");
        }

        private void ChatGroup_SomeoneJoin(IChatUnit obj)
        {
            Console.WriteLine($"ChatGroup_SomeoneJoin:{obj.MemberID}");
        }

        private void IMultimediaManager_AudioPlayed(byte[] obj)
        {
            Console.WriteLine($"IMultimediaManager_AudioPlayed:{obj.Length}");
        }

        private void IMultimediaManager_AudioCaptured(byte[] obj)
        {
            Console.WriteLine($"IMultimediaManager_AudioCaptured:{obj.Length}");
        }

        private void DesktopConnector_Disconnected(ConnectorDisconnectedType type)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<ConnectorDisconnectedType>(this.DesktopConnector_Disconnected), type);
            }
            else
            {
                if (type == ConnectorDisconnectedType.OwnerActiveDisconnect || type == ConnectorDisconnectedType.GuestActiveDisconnect)
                {
                    return;
                }
                MessageBox.Show("断开连接!原因：" + type);
                this.Close();
            }
        }

        private bool Successful;
        private void DesktopConnector_ConnectEnded(OMCS.Passive.ConnectResult result)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<ConnectResult>(this.DesktopConnector_ConnectEnded), result);
            }
            else
            {
                if (result != ConnectResult.Succeed)
                {
                    MessageBox.Show("连接失败!" + result.ToString());
                    this.Close();
                }
                else if (result == ConnectResult.Succeed)
                {
                    Successful = true;
                }
            }
        }

        private void FrmRemoteDesktop_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (Successful)
                {
                    this.desktopConnector.Disconnect();
                    this.desktopConnector.Dispose();
                    Const.IMultimediaManager.ChatGroupEntrance.Exit(ChatType.Audio, this.chatGroup.GroupID);
                    //this.microphoneConnector.Disconnect();
                    //this.microphoneConnector.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        private void FrmRemoteDesktop_Load(object sender, EventArgs e)
        {
            IntPtr sysMenuHandle = Win32Api.GetSystemMenu(this.Handle, false);
            int index = 7;
            Win32Api.InsertMenu(sysMenuHandle, index++, Win32Api.MF_SEPARATOR, 0, null); // -
            Win32Api.InsertMenu(sysMenuHandle, index++, Win32Api.MF_BYPOSITION, 1000, "EasyControl设置");
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Win32Api.WM_SYSCOMMAND)
            {
                //IntPtr sysMenuHandle = Win32Api.GetSystemMenu(m.HWnd, false);
                //Win32Api.CheckMenuItem(sysMenuHandle, IDM_HEIGHT, MF_CHECKED);

                switch (m.WParam.ToInt64())
                {
                    case 1000:
                        new FrmRemoteSetting().ShowDialog();
                        break;
                }
            }
            base.WndProc(ref m);
        }
    }
}
