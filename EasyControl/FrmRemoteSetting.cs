﻿using dTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyControl
{
    public partial class FrmRemoteSetting : Form
    {
        public FrmRemoteSetting()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Const.IMultimediaManager!=null)
                Const.IMultimediaManager.DesktopRegion = new Rectangle(0, 0, txtRectangleW.Text.ToInt(), txtRectangleH.Text.ToInt());
            else
                MessageBox.Show("IMultimediaManager为NULL,设置失败!");
            this.Close();
        }
    }
}
