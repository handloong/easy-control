﻿using dTools;
using EasyControl.Service;
using OMCS.Contracts;
using OMCS.Passive;
using OMCS.Passive.Audio;
using OMCS.Passive.RemoteDesktop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyControl
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            txtUserId.Text = Setting.UserId;
            if (Setting.AutoSaveId)
                Setting.UserId = txtUserId.Text;
        }
        private void txtDestUserId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //to do ask Server
                new FrmRemoteDesktop(txtDestUserId.Text).ShowDialog();
            }
        }

        private void txtDestPassword_KeyDown(object sender, KeyEventArgs e)
        {
            new FrmRemoteDesktop(txtDestUserId.Text).ShowDialog();
        }

        private int retryCount = 0;

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                timer.Stop();

                this.BeginInvoke(new Action(() =>
                {
                    try
                    {
                        Const.IMultimediaManager = MultimediaManagerFactory.GetSingleton();
                        Const.IMultimediaManager.ChannelMode = ChannelMode.P2PChannelFirst;
                        Const.IMultimediaManager.SecurityLogEnabled = false;
                        Const.IMultimediaManager.CameraDeviceIndex = 0;
                        Const.IMultimediaManager.MicrophoneDeviceIndex = 0;
                        Const.IMultimediaManager.DesktopEncodeQuality = 3;
                        Const.IMultimediaManager.AutoAdjustCameraEncodeQuality = false;
                        Const.IMultimediaManager.SpeakerIndex = 0;
                        //if (Const.IMultimediaManager.SystemToken.IsEmpty())
                        //{
                        //    Const.IMultimediaManager.SystemToken = "EasyControl";
                        //}
                        Const.IMultimediaManager.Initialize(txtUserId.Text, "", Setting.ServerIp, 7770);
                        Const.IMultimediaManager.Mute = false;
                        Const.IMultimediaManager.ChatGroupEntrance.Join(ChatType.Audio, $"G_{txtUserId.Text}");
                        picStatus.Image = Properties.Resources.ConnectSuccessful;
                        SetStatus("", false);
                        txtDestUserId.Enabled = true;
                        txtDestUserId.Focus();
                    }
                    catch (Exception ex)
                    {
                        SetStatus(ex.Message, true);
                        picStatus.Image = Properties.Resources.ConnectYellow;
                        timer.Interval = 1000 * 10;
                        timer.Start();
                    }
                }));
            }
            catch
            {
                picStatus.Image = Properties.Resources.ConnectRed;
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText($"{txtUserId.Text}");
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            new FrmSystemSetting().ShowDialog();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("您确定要刷新ID吗?该操作会重启软件,已经连接的将会断开?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Setting.UserId = SnowflakeHelper.NewId().ToString() + StringHelper.BuildRandomString(5).ToUpper();

                Setting.UserId = Guid.NewGuid().ToString("N").ToUpper();

                Application.Restart();
            }
        }

        private void SetStatus(string msg, bool visible = true)
        {
            this.BeginInvoke(new Action(() =>
            {
                lblStatus.Visible = visible;
                lblStatus.Text = msg;
            }));
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (Const.IMultimediaManager != null)
                {
                    Const.IMultimediaManager.ChatGroupEntrance.Exit(ChatType.Audio, $"G_{txtUserId.Text}");
                }
            }
            catch
            {
            }
            Application.Exit();
        }
    }
}
