﻿namespace EasyControl
{
    partial class FrmRemoteDesktop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.desktopConnector = new OMCS.Windows.DesktopConnector();
            this.SuspendLayout();
            // 
            // desktopConnector1
            // 
            this.desktopConnector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.desktopConnector.Location = new System.Drawing.Point(0, 0);
            this.desktopConnector.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.desktopConnector.Name = "desktopConnector1";
            this.desktopConnector.Size = new System.Drawing.Size(282, 253);
            this.desktopConnector.TabIndex = 0;
            // 
            // FrmRemoteDesktop
            // 
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.desktopConnector);
            this.Name = "FrmRemoteDesktop";
            this.ResumeLayout(false);

        }

        #endregion
        private OMCS.Windows.DesktopConnector desktopConnector;
    }
}