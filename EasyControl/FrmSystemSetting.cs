﻿using EasyControl.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyControl
{
    public partial class FrmSystemSetting : Form
    {
        public FrmSystemSetting()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Setting.AutoSaveId = AutoSaveId.Checked;
            Setting.ServerIp = txtServerIp.Text;
            Setting.AutoStart = AutoStart.Checked;

            if (serverIp != txtServerIp.Text)
            {
                if (MessageBox.Show("检测到IP变更,是否重启软件?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Application.Restart();
                }
            }
            dTools.WinformHelper.AutoStart(AutoStart.Checked, Application.ExecutablePath, "EasyControl");
            this.Close();
        }

        private string serverIp;
        private void FrmSetting_Load(object sender, EventArgs e)
        {
            serverIp = Setting.ServerIp;
            txtServerIp.Text = serverIp;
            AutoSaveId.Checked = Setting.AutoSaveId;
            AutoStart.Checked = Setting.AutoStart;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFree_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitee.com/handloong/easy-control");
        }
    }
}
