﻿using dTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyControl.Service
{
    public class Setting
    {
        //txtUserId.Text = EasyINI.Read("UserId", "");
        //    txtPassword.Text = EasyINI.Read("Password", "");

        #region 用户名
        public static string UserId
        {
            get
            {
                var retval = EasyINI.Read("UserId", "");
                if (retval.IsEmpty())
                {
                    return Guid.NewGuid().ToString("N").ToUpper();
                    //return SnowflakeHelper.NewId().ToString() + StringHelper.BuildRandomString(5).ToUpper();
                }
                return retval;
            }
            set
            {
                EasyINI.Write("UserId", value);
            }
        }
        #endregion

        #region 密码
        public static string Password
        {
            get
            {
                var retval = EasyINI.Read("Password", "");
                if (retval.IsEmpty())
                {
                    return dTools.StringHelper.BuildRandomString(10);
                }
                return retval;
            }
            set
            {
                EasyINI.Write("Password", value);
            }
        }
        #endregion

        #region 服务器IP
        public static string ServerIp
        {
            get
            {
                var retval = EasyINI.Read("Server", "");
                if (retval.IsEmpty())
                {
                    return "10.32.36.66";
                }
                return retval;
            }
            set
            {
                EasyINI.Write("Server", value);
            }
        }
        #endregion

        #region 自动保存用户名
        public static bool AutoSaveId
        {
            get => EasyINI.Read("AutoSaveId", true);
            set => EasyINI.Write("AutoSaveId", value);
        }
        #endregion

        #region 开机启动
        public static bool AutoStart
        {
            get => EasyINI.Read("AutoStart", false);
            set => EasyINI.Write("AutoStart", value);
        }
        #endregion
    }
}
