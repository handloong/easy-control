﻿using dTools;
using OMCS;
using OMCS.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyControl.Server
{
    public partial class FrmMain : Form
    {

        private IMultimediaServer _server = null;
        public FrmMain()
        {
            InitializeComponent();

            var port = 7770;
            lblPort.Text = $"Port：{port}";
            lblOnLine.Text = $"OnLine：0";
            _server = MultimediaServerFactory.CreateMultimediaServer(port, new EasyControlUserVerifier());
            _server.UserConnected += Server_UserConnected;
            _server.UserDisconnected += Server_UserDisconnected;
        }

        private void Server_UserDisconnected(string obj)
        {
            this.BeginInvoke(new Action(() =>
            {
                lblOnLine.Text = $"OnLine:{_server.UserCount}";
            }));
        }

        private void Server_UserConnected(string obj)
        {
            this.BeginInvoke(new Action(() =>
            {
                lblOnLine.Text = $"OnLine:{_server.UserCount}";
            }));
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dTools.WinformHelper.AutoStart(true, Application.ExecutablePath, "EasyControlServer");
        }
    }
    public class EasyControlUserVerifier : IUserVerifier
    {
        public bool VerifyUser(string userID, string password)
        {
            return true;
            //if (userID == "dzz" && password == "dzz")
            //    return true;
            //return false;
        }
    }
}
